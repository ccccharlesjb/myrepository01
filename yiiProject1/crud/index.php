<?php include "header.php"; ?>

<ul>
	<li><a href="create.php"><strong>Create</strong></a> - add a user</li>
	<li><a href="pizzaBuilderForm01.php"><strong>Order</strong></a> Build and order a pizza</li>
	<li><a href="read.php"><strong>Read</strong></a> - find a user</li>
	<li><a href="login.php"><strong>Log In</strong></a> log in</li>
</ul>

</br>
<h3>Maintenance:</h3>
<ul>
	<li><a href="addToppings.php"><strong>Add</strong></a> Add Toppings</li>
	<li><a href="editToppingsPage1.php"><strong>Edit</strong></a> Edit Toppings</li>
	<li><a href="deleteToppings.php"><strong>Delete</strong></a> Delete Toppings</li>
</ul>

<?php include "footer.php"; ?>
