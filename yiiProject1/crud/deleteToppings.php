<?php
include "header.php";
include "footer.php";
require 'models/pizzaClass.php';
session_start();
$obj = new pizzaClass();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Delete a Topping</title>
</head>
<body>
<h2> Delete a Topping</h2>

<form method="post">
<table style="width:40%">
    <tr>
        <th>Topping ID</th>
        <th>Topping Name</th> 
        <th>Price</th>
    
    </tr>
    <?php  $toppings = $obj->get_toppings(); ?>
    <?php foreach($toppings as $topping_index=>$topping_content): ?>

    <tr>
        <td><?= "".$toppings[$topping_index]['topping_id']; ?></td>
        <td><?= "". $toppings[$topping_index]['name']; ?></td>
        <td><?= "".$toppings[$topping_index]['price']; ?></td>
        <td> <button name="delete_btn" value="<?= "".
        $toppings[$topping_index]['topping_id']; ?> " onclick="myFunction()">
         Delete </button></td>
       
    </tr>

    <?php endforeach; ?>
    </table>
</form>

<?php 
if(isset($_POST['delete_btn'])) 
{
$obj->delete_topping($_POST['delete_btn']); 
echo "Topping Successfully Deleted";
}
?>



<script>
function myFunction() {
    alert("Topping Successfully Deleted");
}
</script>
    






</body>
</html>