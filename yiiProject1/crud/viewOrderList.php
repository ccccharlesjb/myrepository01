<?php include "header.php"; ?>
<?php require 'models/pizzaClass.php';?>
<?php session_start(); ?>
<?php $obj = new pizzaClass(); ?>
<?php //$obj->get_user_orders(7);?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>View Order List</title>
</head>
<body>


<h2>Name: <?= $_SESSION['last_name']?>, <?= $_SESSION['first_name']?></h2>



<?php  $orders_id = $obj->get_orders_id($_SESSION['user_id']); ?>
<?php foreach($orders_id as $order_id_index=> $order_id_content): ?>
    </br></br></br><h2>Order ID: <?= $orders_id[$order_id_index][0]; ?> </h2>
    <table style="width:40%">
    <tr>
        <th>Order Detail ID</th>
        <th>Topping</th> 
        <th>Price</th>
        <th>Multiplier</th>
        <th>Sub Total</th>
    </tr>
    <?php  $orders = $obj->get_orders_by_order_id($orders_id[$order_id_index][0]); ?>
    <?php foreach($orders as $order_index=>$order_content): ?>

    <tr>
        <td><?= "".$orders[$order_index]['order_detail_id']; ?></td>
        <td><?= "". $obj->convert_ID_to_Name($orders[$order_index]['topping_id'],1); ?></td>
        <td><?= "".$orders[$order_index]['price']; ?></td>
        <td><?= "".$orders[$order_index]['multiplier']; ?></td>
        <td><?= "".$orders[$order_index]['sub_total']; ?></td>
    </tr>

    <?php endforeach; ?>
    </table>
    <h4>Total: <?= $obj->get_order_total($orders_id[$order_id_index][0]); ?> </h4>

<?php endforeach; ?>

    
</body>
</html>


<?php require "footer.php"; ?>
