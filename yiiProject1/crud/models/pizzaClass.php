<?php 
Class PizzaClass
{


function get_orders_by_order_id($order_id)
{
    
    try  
    {
    require "config.php";

    $connection = new PDO($dsn, $username, $password, $options);

    $sql="SELECT * FROM order_details INNER JOIN toppings 
    ON toppings.topping_id = order_details.topping_id
    WHERE order_details.order_id = :order_id";


    $statement = $connection->prepare($sql);
    $statement->bindParam(':order_id', $order_id, PDO::PARAM_STR);
    $statement->execute();
    $result = $statement->fetchAll();

    //print_r($result);

    return $result;
    } 
    catch(PDOException $error)
    {
    echo $sql . "<br>" . $error->getMessage();
    }
    

}

function get_orders_id($user_id)
{
    try  
    {
    require "config.php";

    $connection = new PDO($dsn, $username, $password, $options);

    $sql="SELECT order_id FROM orders 
    WHERE user_id = $user_id";


    $statement = $connection->prepare($sql);
    $statement->bindParam(':user_id', $user_id, PDO::PARAM_STR);
    $statement->execute();
    $result = $statement->fetchAll();

    //print_r($result);

    return $result;
    } 
    catch(PDOException $error)
    {
    echo $sql . "<br>" . $error->getMessage();
    }
}

function get_order_total($order_id)
{
  
    try  
    {
    require "config.php";

    $connection = new PDO($dsn, $username, $password, $options);

    $sql="SELECT SUM(sub_total)
    FROM order_details 
    WHERE order_id=:order_id;";


    $statement = $connection->prepare($sql);
    $statement->bindParam(':order_id', $order_id, PDO::PARAM_STR);
    $statement->execute();
    $result = $statement->fetchAll();

    //print_r($result);

    return $result[0][0];
    } 
    catch(PDOException $error)
    {
    echo $sql . "<br>" . $error->getMessage();
    }

}



function getCategoryID()
{   
    $arr_input = $this->getCategoryTable();
    $arr_output= [];

    foreach($arr_input as $input_name=>$input_content)
        foreach($input_content as $input_index=>$input_value)
            if($input_index==="category_id")
                $arr_output[$input_value]=$input_value;
            
            
    print_r($arr_output);
    return $arr_output;
}


function get_id($table_name,$key,$id)
{
    try  
    {
        require "config.php";

        $connection = new PDO($dsn, $username, $password, $options);

        $sql = "SELECT *
                FROM $table_name
                WHERE $key = :id";

        $statement = $connection->prepare($sql);
        $statement->execute();
        $result = $statement->fetchAll();
        $statement->bindParam(':id', $id, PDO::PARAM_STR);
        print_r($result);
    } 
    catch(PDOException $error)
    {
        echo $sql . "<br>" . $error->getMessage();
    }
}

function get_toppings()
{ 
    try  
    {
        require "config.php";

        $connection = new PDO($dsn, $username, $password, $options);

        $sql = "SELECT *
                FROM toppings ";

        $statement = $connection->prepare($sql);
        $statement->execute();
        $result = $statement->fetchAll();;
        //print_r($result);
        return $result;
    } 
    catch(PDOException $error)
    {
        echo $sql . "<br>" . $error->getMessage();
    }
}

function getToppings_by_ID($cat_id)
{
    try  
    {
        require "config.php";

        $connection = new PDO($dsn, $username, $password, $options);

        $sql = "SELECT tops.* FROM toppings AS tops WHERE tops.category_id = :cat_id";

        $statement = $connection->prepare($sql);
        $statement->bindParam(':cat_id', $cat_id, PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetchAll();

        return $result;
    } 
    catch(PDOException $error)
    {
        echo $sql . "<br>" . $error->getMessage();
    }
}



function get_sizes()
{
    try  
    {
        require "config.php";

        $connection = new PDO($dsn, $username, $password, $options);

        $sql = "SELECT *
                FROM sizes";

        $statement = $connection->prepare($sql);
        $statement->execute();
        $result = $statement->fetchAll();
       
       // print_r($result);
        return $result;
    } 
    catch(PDOException $error)
    {
        echo $sql . "<br>" . $error->getMessage();
    }

}

function get_categories()
{
    try  
    {
        require "config.php";

        $connection = new PDO($dsn, $username, $password, $options);

        $sql = "SELECT category_id,
                    category_name

                FROM categories";

        $statement = $connection->prepare($sql);
        $statement->execute();
        $result = $statement->fetchAll();
        
        /*
        foreach($result as $result_name=> $result_content)
            {   
                echo "</br> INDEX $result_name  and VALUE = " . $result[$result_name]['category_name'] . " and ID: " . $result[$result_name]['category_id'];
            }
        */
        //print_r($result);
        return $result;
    } 
    catch(PDOException $error)
    {
        echo $sql . "<br>" . $error->getMessage();
    }

}


public function get_receipt($arr_input,$price_multiplier=1)
{
    $categories=$this->get_categories();
    $toppings=$this->get_toppings();
    $arr_input_length= count($arr_input);
    $output="";
    
    $header="Size";
    $content ="</br>" . $this->convert_ID_to_Name($price_multiplier, 0);       
    $output=$header . "" . $content; 

    foreach($categories as $category_name => $category_content)
    {

        $header="";
        $content="";

        foreach($arr_input as $input_index => $input_value)
        {
         if($categories[$category_name]['category_id']==$this->determine_catID($arr_input[$input_index]))
            {
             $header = "</br> </br>" . $categories[$category_name]['category_name'];
             $content.= "</br> " . $this->convert_ID_to_Name($arr_input[$input_index],1) . "      PHP " .
             $this->convert_toppingsID_to_value($arr_input[$input_index],1) * $this->convert_toppingsID_to_value($price_multiplier,0);
            
            }
        }

        $output.=$header . "" . $content;
     }   
     echo $output;
}

function getLatestOrderID()
{
    
    
    try  
    {
        require "config.php";

        $connection = new PDO($dsn, $username, $password, $options);

        $sql= "SELECT order_id FROM orders ORDER BY order_id DESC LIMIT 1";

        $statement = $connection->prepare($sql);
        
        $statement->execute();
        $result = $statement->fetchAll();
        
        return $result[0]['order_id'];
    } 
    catch(PDOException $error)
    {
        echo $sql . "<br>" . $error->getMessage();
    }  
}

function getLatestCreated_at()
{
    
    
    try  
    {
        require "config.php";

        $connection = new PDO($dsn, $username, $password, $options);

        $sql= "SELECT created_at FROM orders ORDER BY created_at DESC LIMIT 1";

        $statement = $connection->prepare($sql);
        
        $statement->execute();
        $result = $statement->fetchAll();
        
        return $result[0]['created_at'];
    } 
    catch(PDOException $error)
    {
        echo $sql . "<br>" . $error->getMessage();
    }  
}

function getOrders()
{
    try  
    {
        require "config.php";

        $connection = new PDO($dsn, $username, $password, $options);

        $sql = "SELECT *
                FROM orders";

        $statement = $connection->prepare($sql);
        $statement->execute();
        $result = $statement->fetchAll();;
        print_r($result);
        return $result;
    } 
    catch(PDOException $error)
    {
        echo $sql . "<br>" . $error->getMessage();
    }   
}

function insert_orderList($arr_input,$price_multiplier)
{
    $order_id = (int)$this->getLatestOrderID();
    $created_at = $this->getLatestCreated_at();
    foreach($arr_input as $arr_index=>$arr_content)
    {
        $this->insert_single_orderList($order_id,
        (int)$arr_content,
        $created_at,
        $price_multiplier);
    }
}

function insert_single_orderList($order_id,$topping_id,$created_at,$multiplier=1)
{

    $price = $this->convert_toppingsID_to_value($topping_id,1);
    $multiplier = $this->convert_toppingsID_to_value($multiplier,0);
    $sub_total = $price * $multiplier;
    
    
    //echo "</br> </br> order detail ID: NULL </br> Order ID: $order_id </br> Topping ID: $topping_id 
    //</br> Price: $price </br> Multiplier: $multiplier </br> Sub Total: $sub_total 
    //</br> Created At: $created_at </br> </br>";
    try  
    {
        require "config.php";

        $connection = new PDO($dsn, $username, $password, $options);

        $sql= "INSERT INTO pizza_builder.order_details (
            order_id,
            topping_id,
            price,
            multiplier,
            sub_total,
            created_at) 
            VALUES (
                :order_id,
                :topping_id,
                :price,
                :price_multiplier,
                :sub_total,
                :created_at)";
                
        $insert_arr = [
                'order_id' => $order_id,
                'topping_id' => $topping_id,
                'price' => $price,
                'price_multiplier' => $multiplier,
                'sub_total' => $sub_total,
                'created_at' => $created_at,             
        ];
        $statement = $connection->prepare($sql);
        $statement->bindParam(':order_id', $order_id, PDO::PARAM_STR);
        $statement->bindParam(':topping_id', $topping_id, PDO::PARAM_STR);
        $statement->bindParam(':price', $price, PDO::PARAM_STR);
        $statement->bindParam(':multiplier', $multiplier, PDO::PARAM_STR);
        $statement->bindParam(':sub_total', $sub_total, PDO::PARAM_STR);
        $statement->bindParam(':created_at', $created_at, PDO::PARAM_STR);
        
        $statement->execute($insert_arr);

    } 
    catch(PDOException $error)
    {
        echo $sql . "<br>" . $error->getMessage();
    }
}

function insert_order($id)
{
    $id=(int)$id;
    $date = date('Y-m-d H:i:s');
    
    try  
    {
        require "config.php";

        $connection = new PDO($dsn, $username, $password, $options);

        $sql= "INSERT INTO pizza_builder.orders (user_id) VALUES (:user_id)";
        
        $statement = $connection->prepare($sql);
        $statement->bindParam(':user_id', $id, PDO::PARAM_STR);
        $statement->execute();
    } 
    catch(PDOException $error)
    {
        echo $sql . "<br>" . $error->getMessage();
    }
}

function insert_topping($name,$price,$category_id)
{

    $price=(float)$price;
    $category_id=(int)$category_id;
    try  
    {
        require "config.php";

        $connection = new PDO($dsn, $username, $password, $options);

        $sql= "INSERT INTO pizza_builder.toppings (category_id, name, price) VALUES (:category_id, :name, :price)";
        
        $statement = $connection->prepare($sql);
        $new_user = array(
            "category_id" => $category_id,
            "name"  => $name,
            "price"  => $price);

        $statement->bindParam('category_id', $category_id, PDO::PARAM_STR);
        $statement->bindParam('name', $name, PDO::PARAM_STR);
        $statement->bindParam('price', $price, PDO::PARAM_STR);
        $statement->execute($new_user);
    } 
    catch(PDOException $error)
    {
        echo $sql . "<br>" . $error->getMessage();
    }
}

function delete_topping($topping_id)
{

    $topping_id = (int) $topping_id;
    try  
    {
        require "config.php";

        $connection = new PDO($dsn, $username, $password, $options);

        $sql= "DELETE FROM pizza_builder.toppings 
            WHERE  topping_id = :topping_id;" ;
        
        $statement = $connection->prepare($sql);
        $statement->bindParam(':topping_id', $topping_id, PDO::PARAM_STR);
        $statement->execute();
    } 
    catch(PDOException $error)
    {
        echo $sql . "<br>" . $error->getMessage();
    }
}


function convert_toppingsID_to_value($id,$category=0)
{    
    // category = 0 -> convert size id to value
    // category = 1 -> convert toppings to value


    $id=(float)$id;   
    $category=(float)$category;
    
    try  
    {
        require "config.php";

        $connection = new PDO($dsn, $username, $password, $options);

        if($category==0)
        $sql = "SELECT value FROM sizes  WHERE size_id = :id";
        else if($category==1) 
        $sql = "SELECT price FROM toppings  WHERE topping_id = :id";
        else
        {
            $sql="";
            echo "Invalid input in 2nd argument";
        }

        $statement = $connection->prepare($sql);
        $statement->bindParam(':id', $id, PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetchAll();

        return $result[0][0];
    } 
    catch(PDOException $error)
    {
        echo $sql . "<br>" . $error->getMessage();
    }
}    

function convert_ID_to_Name($id,$category=0)
{
    $id=(int)$id;   
    $category=(float)$category;
    
    try  
    {
        require "config.php";

        $connection = new PDO($dsn, $username, $password, $options);

        if($category==0)
        $sql = "SELECT value FROM sizes  WHERE size_id = :id";
        else if($category==1) 
        $sql = "SELECT name FROM toppings  WHERE topping_id = :id";
        else
        {
            $sql="";
            echo "Invalid input in 2nd argument";
        }

        $statement = $connection->prepare($sql);
        $statement->bindParam(':id', $id, PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetchAll();
        
        return "".$result[0][0];
    } 
    catch(PDOException $error)
    {
        echo $sql . "<br>" . $error->getMessage();
    }
}

function determine_catID($id)
{
    $id=(int)$id;   
    
    try  
    {
        require "config.php";

        $connection = new PDO($dsn, $username, $password, $options);

    
        $sql = "SELECT category_id FROM toppings  WHERE topping_id = :id";
      

        $statement = $connection->prepare($sql);
        $statement->bindParam(':id', $id, PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetchAll();
        
        return "".$result[0][0];
    } 
    catch(PDOException $error)
    {
        echo $sql . "<br>" . $error->getMessage();
    }
}

function computeTotalOrder($array_input,$pizza_multiplier=1)
{
        

        $total=0;
        
        foreach($array_input as $input_index=>$input_value)
        { 
            $total+= $this->convert_toppingsID_to_value($array_input[$input_index],1)*
            $this->convert_toppingsID_to_value($pizza_multiplier,0);
            
        }
        


       return $total;
        

}


  



}







?>
