<?php

/**
 * Use an HTML form to create a new entry in the
 * users table.
 *
 */


if (isset($_POST['submit'])) {
    require "config.php";
    
    
    
    if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) 
    {
        $emailErr = "Invalid email format"; 
        
        
    }
    else
  {  
      try  
      {
            $connection = new PDO($dsn, $username, $password, $options);
        
            $new_user = array(
                "first_name" => $_POST['first_name'],
                "last_name"  => $_POST['last_name'],
                "email"  => $_POST['email'],
                "password"       => md5($_POST['password']),
                "address"     => $_POST['address'],
                "contact"       => $_POST['contact'],
                "created_at"  => date('Y-m-d H:i:s'),
                "updated_at"  => date('Y-m-d H:i:s')
                );

            $sql = sprintf(
                "INSERT INTO %s (%s) values (%s)",
                "users",
                implode(", ", array_keys($new_user)),
                ":" . implode(", :", array_keys($new_user))
                );

                $statement = $connection->prepare($sql);
                $statement->execute($new_user);

                
                

     } 
        catch(PDOException $error) 
        {
            echo $sql . "<br>" . $error->getMessage();
        }
    }
}
?>

<?php require "header.php"; ?>
<?php if(isset($statement)):?>
    <?php if (isset($_POST['submit']) && $statement) { ?>
        <blockquote><?php echo "{$_POST['first_name']} {$_POST['last_name']}"; ?> Customer was successfully added.</blockquote>
    
<?php } ?>
<?php endif; ?>
<h4><font color="red"><i><?php echo $emailErr; ?></i></font></h4>

<h2>Add a Customer</h2>

<form method="post">
    <label for="first_name">First Name</label>
    <input type="text" name="first_name" id="first_name">
    <label for="last_name">Last Name</label>
    <input type="text" name="last_name" id="last_name">
    <label for="email">Email Address</label>
    <input type="text" name="email" id="email">
    <label for="address">Address</label>
    <input type="text" name="address" id="address">
    <label for="contact">Contact</label>
    <input type="text" name="contact" id="contact">
    <label for="password">Password</label>
    <input type="password" name="password" id="password">
    <input type="submit" name="submit" value="Submit">
</form>

<a href="index.php">Back to home</a>

<?php require "footer.php"; ?>
