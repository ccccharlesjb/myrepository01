<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p> 
    A ticketing system allows IT support to be organized, focused, efficient, and effective.

This directly impacts costs and revenues, customer retention, and public brand image.

Ultimately, ticketing systems are a means to support and help you deal with any issues/incidents in your organization, managing the incidents from the moment they’re captured through to their resolution.
 </p>

    <code><?= __FILE__ ?></code>

    <img src="/image/about-background.jpg">
</div>
