<?php

/* @var $this \yii\web\View */
/* @var $content string */

use kartik\sidenav\SideNav;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\Tickets;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?php

if(!Yii::$app->user->getIsGuest())
{
    $model = Tickets::find()
    ->where(['user_id' => Yii::$app->user->identity->user_id])->orWhere(['updated_by' => Yii::$app->user->identity->user_id])->all();
    $dashboard_counter = Html::tag('span', count($model) . " tickets" , ['class' => 'label ticket-counter']);
}
else
{
    $dashboard_counter="";
}
    
?>

<?php 
 echo SideNav::widget([
    'type' => SideNav::TYPE_DEFAULT,
    'heading' => '<img src="/image/yondu.jpg" class = "company_logo">',
    'encodeLabels' => false,
    'items' => [
        [
            'url' => '/web/',
            'label' => 'Home',
            'icon' => 'home'
        ],
        [
            'label' => 'Dashboard ' . $dashboard_counter,
            'icon' => 'th-list',
            'url' => '/web/tickets/',
        ],
        [
            'label' => 'Users',
            'icon' => 'user',
            'items' => [
                ['label' => 'View users', 'icon'=>'eye-open', 'url'=>'/web/users/index'],
                ['label' => 'Add a user', 'icon'=>'plus', 'url'=>'/web/users/create'],
            ],
        ],
        [
            'label' => 'Tickets',
            'icon' => 'tag',
            'items' => [
                ['label' => 'View Tickets', 'icon'=>'eye-open', 'url'=>'/web/tickets/'],
                ['label' => 'Create Tickets', 'icon'=>'plus', 'url'=>'/web/tickets/create/'],
            ],
        ],
        [
            'label' => 'Help',
            'icon' => 'question-sign',
            'items' => [
                ['label' => 'About', 'icon'=>'info-sign', 'url'=>'/web/site/about'],
                ['label' => 'Contact', 'icon'=>'phone', 'url'=>'/web/site/contact'],
            ],
        ],
    ],
]);

NavBar::begin([
    'brandLabel' => "",//Yii::$app->name,
    'brandUrl' => "",//Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);

echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => [
        Yii::$app->user->isGuest ? (
            ['label' => 'Login', 'url' => ['/site/login']]
        ) : (
            '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->email . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>'
        )
    ],
]);
NavBar::end();
?>



<div class="wrap">
    <?php

/*
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'Users', 'url' => ['users/index']],
            ['label' => 'Tickets', 'url' => ['tickets/index']],
            ['label' => 'Contact', 'url' => ['/site/contact']],
            ['label' => 'About', 'url' => ['/site/about']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->email . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
        */

        
      
       

    
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; By Jethro Belamide <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
