<?php
use yii\helpers\Html;
use yii\grid\GridView;
?>

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ticket_id',
            'description',
            'user_id',
            'updated_by',
            'categoriesName',
            'subCategoriesName',
            //'ticket_type',
            //'priority_id',
            //'notes:ntext',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>



 <h1>My Tickets</h1>

<?php

echo GridView::widget([
    'dataProvider' => $sqlProviderForUser,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'ticket_id',
        'ticket_type',
        'description',
        [ 'label' => 'Created by',
            'attribute' => 'name',
        ],
        'updated_by',
        'priority',
        //'ticket_type',
        //'priority_id',
        //'notes:ntext',
        //'created_at',
        //'updated_at',

        ['class' => 'yii\grid\ActionColumn',
        'buttons' => [
            'update' => function($url, $model) {
                $options = [
                    'title' => Yii::t('yii', 'Update'),
                    'aira-label' => Yii::t('yii', 'Update'),
                ];


                $url = '/web/tickets/update?id=' . $model['ticket_id'];

                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options );
            },
            'view' => function($url, $model) {
                $options = [
                    'title' => Yii::t('yii', 'View'),
                    'aira-label' => Yii::t('yii', 'View'),
                ];


                $url = '/web/tickets/view?id=' . $model['ticket_id'];

                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options );
            }
            ,'delete' => function($url, $model) {
                $options = [
                    'title' => Yii::t('yii', 'delete'),
                    'aira-label' => Yii::t('yii', 'delete'),
                ];


                $url = '/web/tickets/delete?id=' . $model['ticket_id'];

                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options );
            }
        ]
    ],
    ],
]);
?>

 <h1>Tickets</h1>
<?php
echo GridView::widget([
    'dataProvider' => $sqlProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'ticket_id',
        'description',
        [ 'label' => 'Created by',
            'attribute' => 'name',
        ],
        'updated_by',
        'priority',
        //'ticket_type',
        //'priority_id',
        //'notes:ntext',
        //'created_at',
        //'updated_at',

        ['class' => 'yii\grid\ActionColumn',
            'buttons' => [
                'update' => function($url, $model) {
                    $options = [
                        'title' => Yii::t('yii', 'Update'),
                        'aira-label' => Yii::t('yii', 'Update'),
                    ];


                    $url = '/web/tickets/update?id=' . $model['ticket_id'];

                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options );
                },
                'view' => function($url, $model) {
                    $options = [
                        'title' => Yii::t('yii', 'View'),
                        'aira-label' => Yii::t('yii', 'View'),
                    ];


                    $url = '/web/tickets/view?id=' . $model['ticket_id'];

                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options );
                }
                ,'delete' => function($url, $model) {
                    $options = [
                        'title' => Yii::t('yii', 'delete'),
                        'aira-label' => Yii::t('yii', 'delete'),
                    ];


                    $url = '/web/tickets/delete?id=' . $model['ticket_id'];

                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options );
                }
            ]
        ],
    ],
]);
?>