<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\Tickets */
/* @var $form yii\widgets\ActiveForm */


$category_list     = ArrayHelper::map($categories_model,"category_id", 'name');                                
$sub_category_list = ArrayHelper::map($sub_categories_model,"sub_category_id", 'description');
$priorities_list   = ArrayHelper::map($priorities_model,"priority_id", 'description');

?>


<?php if(isset($form_selected) && $form_selected == 'create'): ?>
    <div class="tickets-form col-lg-5">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput(['maxlength' => true , 'value' => Yii::$app->user->identity->id, 'readOnly' => true]) ?>

    <?=""; $form->field($model, 'updated_by')->textInput(['maxlength' => true, 'value' => Yii::$app->user->identity->id   , 'readOnly' => true]) ?>
    
      <?= $form->field($model, 'category_id')->dropDownList($category_list, ['prompt' => 'Select a Category',  
    'onchange' => '
        $.post( "'.Yii::$app->urlManager->createUrl('tickets/getsc?id=').'"+$(this).val(), function( data ) {
            $( "select#subCategories" ).html( data );
        });']) ?>
   
   

    <?= $form->field($model, 'sub_category_id')->dropDownList($sub_category_list, ['id' => 'subCategories','prompt' => 'Select a Sub Category']) ?>
    <!-- PLEASE READ!! Dropdown([INPUT ARRAY], [THIS IS WHERE YOU INPUT OPTIONS FOR THE DROPDOWN (ARRAY OPTION])--> 
   
    <?= $form->field($model, 'priority_id')->dropDownList($priorities_list, ['prompt' => 'Select a Priority',]) ?>

    <?= $form->field($model, 'description')->textArea(['maxlength' => true, 'rows' => 6]) ?>

    <?= $form->field($model, 'notes')->textarea(['rows' => 6]) ?>

    <?= "";//$form->field($model, 'created_at')->textInput(['value' => date('Y-m-d H:i:s'), 'readOnly' => true]) ?>

    <?="";// $form->field($model, 'updated_at')->textInput(['value' => '', 'readOnly' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    </div>
<?php elseif(isset($form_selected) && $form_selected == 'update'): ?>

<?php
   
     
?>


    <div class="tickets-form col-lg-6">

    <?php $form = ActiveForm::begin(); ?>

    </br> </br> </br> 
    <table class="my-table">
    <tbody>
        <tr>
            <th> Created By
            </th>
        </tr>
        <tr>
            <td class="info-left"><?= $form->field($model, 'user_id')->textInput(['maxlength' => 
            true , 'value' => $ticket_creator->user_id  , 'readOnly' => true]) ?>
            </td>
            <td class="info-highlight"><?= $form->field($model, 'Name')->textInput(['maxlength' =>
             true , 'value' => $ticket_creator->first_name . " " .
             $ticket_creator->last_name ,
                'readOnly' => true]) ?>
            </td>
            <td> <?= $form->field($model, 'created_at')->textInput(['value' => $selected_record->created_at , 'readOnly' => true]) ?>
          </td>
        </tr>

    </tbody>
 </table>


    <table class="my-table">
    <tbody>
        <tr>
            <th> Updated By
            </th>
        </tr>
        <tr>
            <td class="info-left"><?= $form->field($model, 'user_id')->textInput(['maxlength' => 
            true , 'value' => Yii::$app->user->identity->id  , 'readOnly' => true]) ?>
            </td>
            <td class="info-highlight"><?= $form->field($model, 'Name')->textInput(['maxlength' =>
             true , 'value' => Yii::$app->user->identity->first_name . " " .
             Yii::$app->user->identity->last_name ,
                'readOnly' => true]) ?>
            </td>
            <td>   <?= $form->field($model, 'updated_at')->textInput(['value' => date('Y-m-d H:i:s'), 'readOnly' => true]) ?>
            </td>
        </tr>

    </tbody>
 </table>


      <?= $form->field($model, 'category_id')->dropDownList($category_list, ['prompt' => 'Select a Category',  
    'onchange' => '
        $.post( "'.Yii::$app->urlManager->createUrl('tickets/getsc?id=').'"+$(this).val(), function( data ) {
            $( "select#subCategories" ).html( data );
        });']) ?>
   
   

    <?= $form->field($model, 'sub_category_id')->dropDownList($sub_category_list, ['id' => 'subCategories','prompt' => 'Select a Sub Category']) ?>
    <!-- PLEASE READ!! Dropdown([INPUT ARRAY], [THIS IS WHERE YOU INPUT OPTIONS FOR THE DROPDOWN (ARRAY OPTION])--> 
   
    <?= $form->field($model, 'priority_id')->dropDownList($priorities_list, ['prompt' => 'Select a Priority',]) ?>

    <?= $form->field($model, 'description')->textArea(['maxlength' => true, 'rows' => 6]) ?>

    <?= $form->field($model, 'notes')->textarea(['rows' => 6]) ?>

    <?="";// $form->field($model, 'created_at')->textInput(['value' => $selected_record->created_at , 'readOnly' => true]) ?>

    <?="";// $form->field($model, 'updated_at')->textInput(['value' => date('Y-m-d H:i:s'), 'readOnly' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    </div>
<?php else: ?>
    <?php echo "Invalid input for form_selected varaible"; ?>
    
<?php endif; ?>