<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tickets */

$this->title = 'Update Tickets: ' . $model->ticket_id;
$this->params['breadcrumbs'][] = ['label' => 'Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ticket_id, 'url' => ['view', 'id' => $model->ticket_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tickets-update">

    <h1><?=Html::encode($this->title) ?></h1>

    <?= $this->render('_updateForm', [
        'model' => $model,
        'categories_model' => $categories_model,
        'priorities_model' => $priorities_model,
        'sub_categories_model' => $sub_categories_model,
        'selected_record' => $selected_record,
        'ticket_creator' => $ticket_creator,
    ]) ?>

</div>
