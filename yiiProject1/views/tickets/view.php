<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Tickets */

$this->title = "Ticket ID: " . $model->ticket_id;
$this->params['breadcrumbs'][] = ['label' => 'Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

 <p>
        <?= Html::a('Update', ['update', 'id' => $model->ticket_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ticket_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>



<fieldset style ="border: 1px solid black;">
    <p class = "fieldset-background"> Ticket Details </p>

<div class ="col-lg-12">
 <h2> <?="Ticket ID: " . $model->ticket_id; ?> <h2>
</div>

<div class = "col-lg-3">
<label class = "control-label">Category ID</label>
<input type="text" class= "form-control" value = "<?=$model->category_id; ?>" readOnly> 
</div>



<div class = "col-lg-3">
<label class = "control-label">Sub Category ID</label>
<input type="text" class= "form-control" value = "<?=$model->sub_category_id; ?>"readOnly> 
</div>

<div class = "col-lg-3">
<label class = "control-label">Ticket Type</label>
<input type="text" class= "form-control" value = "<?=$model->ticket_type; ?>"readOnly> 
</div>

<div class = "col-lg-3">
    <label class = "control-label">Priority ID</label>
<input type="text" class= "form-control" value = "<?=$model->priority_id; ?>"readOnly> 
</div>

<div class ="col-lg-12">
 <br>
</div>

<div class = "col-lg-6">
<h3>Created By</h3>
</div>

<div class = "col-lg-6">
<h3>Updated By</h3>
</div>

<div class = "col-lg-3">
<label class = "control-label">User ID</label>
<input type="text" class= "form-control" value = "<?=$model->user_id; ?>" readonly> 
</div>

<div class = "col-lg-3">
<label class = "control-label">Name</label>
<input type="text" class= "form-control" value = "<?=$creator_record->first_name . 
" " . $creator_record->last_name;?>"readOnly> 
</div>


<div class = "col-lg-3">
<label class = "control-label">User ID</label>
<input type="text" class= "form-control" value = "<?=$model->updated_by; ?>"readOnly> 
</div>

<div class = "col-lg-3">
<label class = "control-label">Name</label>
<input type="text" class= "form-control" value = "<?=$updator_record->first_name . " " . 
$updator_record->last_name; ?>"readOnly> 
</div>

<div class = "col-lg-6">
<label class = "control-label">Created At</label>
<input type="text" class= "form-control" value = "<?=$model->created_at; ?>"readOnly> 
</div>

<div class = "col-lg-6">
<label class = "control-label">Updated At</label>
<input type="text" class= "form-control" value = "<?=$model->updated_at; ?>"readOnly> 
</div>


<div class ="col-lg-12">
 <br>
</div>

</fieldset>

<div class ="col-lg-12">
 <br>
</div>

<div class ="col-lg-6" >
    <fieldset style ="border: 1px solid black;" >
        <p class = "fieldset-background" > Description </p>
        <div  style="padding: 10px">
            <textarea class= "form-control" rows = "6"; readOnly > <?=$model->description; ?> </textarea> 
        </div>
    </fieldset>
</div>

<div class ="col-lg-6">
    <fieldset style ="border: 1px solid black; ">
        <p class = "fieldset-background"> Notes </p>
        <div  style="padding: 10px">
            <textarea class= "form-control" rows = "6"; readOnly> <?=$model->notes; ?> </textarea> 
        </div>
    </fieldset>
</div>






