<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tickets */

$this->title = 'Create Tickets';
$this->params['breadcrumbs'][] = ['label' => 'Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tickets-create">


    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_createForm', [
        'model' => $model,
        'categories_model' => $categories_model,
        'sub_categories_model' => $sub_categories_model,
        'priorities_model' => $priorities_model,
    ]) ?>

</div>


