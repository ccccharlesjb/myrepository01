<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\Tickets */
/* @var $form yii\widgets\ActiveForm */


$category_list     = ArrayHelper::map($categories_model,"category_id", 'name');                                
$sub_category_list = ArrayHelper::map($sub_categories_model,"sub_category_id", 'description');
$priorities_list   = ArrayHelper::map($priorities_model,"priority_id", 'description');

?>


  <!-- <div class="tickets-form col-lg-6"> -->

   <?php $form = ActiveForm::begin(); ?>

   <fieldset style ="border: 1px solid black;">
    <p class = "fieldset-background"> Ticket Details </p>

<div class = "col-lg-6">
<h3>Created By</h3>
</div>

<div class = "col-lg-6">
<h3>Updated By</h3>
</div>

<div class = "col-lg-3">
<?= $form->field($model, 'user_id')->textInput(['maxlength' => true, 'readOnly' => true]) ?>
</div>

<div class = "col-lg-3">
<label class = "control-label">Name</label>
<input type="text" class= "form-control" value = "<?=$ticket_creator->first_name . 
" " . $ticket_creator->last_name; ?>"readOnly> 
</div>


<div class = "col-lg-3">
<?= $form->field($model, 'updated_by')->textInput(['maxlength' => true, 'value' => Yii::$app->user->identity->user_id, 'readOnly' => true])->label("User ID"); ?>
</div>

<div class = "col-lg-3">
<label class = "control-label">Name</label>
<input type="text" class= "form-control" value = "<?= Yii::$app->user->identity->first_name . 
" " .  Yii::$app->user->identity->last_name; ?>"readOnly> 
</div>

<div class = "col-lg-6">
<?= $form->field($model, 'created_at')->textInput(['readOnly' => true]) ?>
</div>

<div class = "col-lg-6">
<?= $form->field($model, 'updated_at')->textInput(['value' => date('Y-m-d H:i:s'), 'readOnly' => true]) ?>
</div>
<div class = "col-lg-3">
<?= $form->field($model, 'category_id')->dropDownList($category_list, ['prompt' => '-Select a Category-',  
                'onchange' => '
                $.post( "'.Yii::$app->urlManager->createUrl('tickets/getsc?id=').'"+$(this).val(), function( data ) {
                $( "select#subCategories" ).html( data );
                });']) ?>
</div>



<div class = "col-lg-3">
 <?= $form->field($model, 'sub_category_id')->dropDownList($sub_category_list, ['id' => 'subCategories','prompt' => '-Select a Sub Category-']) ?>
</div>

<div class = "col-lg-3">
<?= $form->field($model, 'ticket_type')->dropDownList([ 'incident' => 'Incident', 'problem' => 'Problem', 'service_request' => 'Service request', ], ['prompt' => '']) ?>
</div>

<div class = "col-lg-3">
<?= $form->field($model, 'priority_id')->dropDownList($priorities_list, ['prompt' => '-Select a Priority-',]) ?>
</div>

</fieldset>

<div class ="col-lg-12">
<br> <br>
</div>


<fieldset style ="border: 1px solid black;">
    <p class = "fieldset-background"> Description </p>
<div class = "col-lg-12">
<div  style="padding: 2px">
<?= $form->field($model, 'description')->textarea(['maxlength' => true, 'rows' => 6])->label(false)?>
            </div>
</div>
</fieldset>

<div class ="col-lg-12">
<br> <br>
</div>

<fieldset style ="border: 1px solid black;">
    <p class = "fieldset-background"> Notes </p>
<div class = "col-lg-12">
<?= $form->field($model, 'notes')->textarea(['rows' => 6])->label(false) ?>
</div>
</fieldset>

<div class ="col-lg-12">
<br>
</div>


<div class="form-group col-lg-12">
    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>
