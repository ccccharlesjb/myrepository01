<?php
use yii\helpers\Html;
use yii\grid\GridView;
?>

<p>
        <?= Html::a('Create Tickets', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


 <h1>My Tickets</h1>


<?php
echo GridView::widget([
    'dataProvider' => $sqlProviderForUser,
   
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'ticket_id',
        'ticket_type',
        'description',
        'created_by',
       /* [ 'label' => 'Created by',
            'attribute' => 'name',
        ],*/
        'updated_by',
        'priority',
        //'ticket_type',
        //'priority_id',
        //'notes:ntext',
        //'created_at',
        //'updated_at',

        ['class' => 'yii\grid\ActionColumn',
        'buttons' => [
            'update' => function($url, $model) {
                $options = [
                    'title' => Yii::t('yii', 'Update'),
                    'aira-label' => Yii::t('yii', 'Update'),
                ];


                $url = '/web/tickets/update?id=' . $model['ticket_id'];

                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options );
            },
            'view' => function($url, $model) {
                $options = [
                    'title' => Yii::t('yii', 'View'),
                    'aira-label' => Yii::t('yii', 'View'),
                ];


                $url = '/web/tickets/view?id=' . $model['ticket_id'];

                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options );
            }
            ,'delete' => function($url, $model) {
                $options = [
                    'title' => Yii::t('yii', 'delete'),
                    'aira-label' => Yii::t('yii', 'delete'),
                    'data' => [
                        'confirm' => 'Are you absolutely sure ? You will lose all the information about this ticket with this action.',
                        'method' => 'post',
                              ],
                ];


                $url = '/web/tickets/delete?id=' . $model['ticket_id'];

                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
            }
        ]
    ],
    ],
]);
?>
<hr>
 <h1>Tickets</h1>
 
<?php
echo GridView::widget([
    'dataProvider' => $sqlProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'ticket_id',
        'description',
        'created_by',
        /*
        [ 'label' => 'Created by',
            'attribute' => 'name',
        ],*/
        'updated_by',
        'priority',
        //'ticket_type',
        //'priority_id',
        //'notes:ntext',
        //'created_at',
        //'updated_at',

        ['class' => 'yii\grid\ActionColumn',
            'buttons' => [
                'update' => function($url, $model) {
                    $options = [
                        'title' => Yii::t('yii', 'Update'),
                        'aira-label' => Yii::t('yii', 'Update'),
                    ];


                    $url = '/web/tickets/update?id=' . $model['ticket_id'];

                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options );
                },
                'view' => function($url, $model) {
                    $options = [
                        'title' => Yii::t('yii', 'View'),
                        'aira-label' => Yii::t('yii', 'View'),
                    ];


                    $url = '/web/tickets/view?id=' . $model['ticket_id'];

                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options );
                }
                ,'delete' => function($url, $model) {
                    $options = [
                        'title' => Yii::t('yii', 'delete'),
                        'aira-label' => Yii::t('yii', 'delete'),
                        'data' => [
                            'confirm' => 'Are you absolutely sure ? You will lose all the information about this ticket with this action.',
                            'method' => 'post',
                                  ],
                    ];


                    $url = '/web/tickets/delete?id=' . $model['ticket_id'];

                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options );
                }
            ]
        ],
    ],
]);
?>