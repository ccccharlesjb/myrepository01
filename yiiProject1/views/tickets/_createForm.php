<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\Tickets */
/* @var $form yii\widgets\ActiveForm */


$category_list     = ArrayHelper::map($categories_model,"category_id", 'name');                                
$sub_category_list = ArrayHelper::map($sub_categories_model,"sub_category_id", 'description');
$priorities_list   = ArrayHelper::map($priorities_model,"priority_id", 'description');

?>




    <?php $form = ActiveForm::begin(); ?>

<fieldset style ="border: 1px solid black;">
    <p class = "fieldset-background"> Ticket Details </p>

    
            
            <div class = "col-lg-3">
                <?= $form->field($model, 'ticket_type')->dropDownList([ 'incident' => 'Incident', 'problem' => 'Problem', 'service_request' => 'Service request', ], ['prompt' => '-Select a Ticket Type-']) ?>
            </div>

            <div class = "col-lg-3">
                <?= $form->field($model, 'category_id')->dropDownList($category_list, ['prompt' => '-Select a Category-',  
                'onchange' => '
                $.post( "'.Yii::$app->urlManager->createUrl('tickets/getsc?id=').'"+$(this).val(), function( data ) {
                $( "select#subCategories" ).html( data );
                });']) ?>
            </div>

            <div class = "col-lg-3">
                <?= $form->field($model, 'sub_category_id')->dropDownList($sub_category_list, ['id' => 'subCategories','prompt' => '-Select a Sub Category-']) ?>
            </div>

            <div class = "col-lg-3">
                <?= $form->field($model, 'priority_id')->dropDownList($priorities_list, ['prompt' => '-Select a Priority-',]) ?>
            </div>  
                
            <div class = "col-lg-12">
            <b> Created By </b> <br> <br>
            </div>
           
           
            
            <div class = "col-lg-3">
            <?= $form->field($model, 'user_id')->textInput(['maxlength' => true,
            'value' => Yii::$app->user->identity->user_id,
            'readOnly' => true,
            ]) ?>
            </div>
                
            <div class = "col-lg-3">
            <label class="control-label"> Name</label>
            <input class = "form-control" value = "<?=Yii::$app->user->identity->first_name . " " . Yii::$app->user->identity->last_name ?>" readOnly >
            </div>
            <div class = "col-lg-3">
            <label class="control-label"> Created At</label>
            <?=$form->field($model, 'created_at')->textInput(['value' => date('Y-m-d H:i:s'), 'readOnly' => true])->label(false) ?>
            </div>

             <div class = "col-lg-3">
             </div>

        

     
 </fieldset>

<br>
 <fieldset style ="border: 1px solid black;">
    <p class = "fieldset-background"> Description </p>
    <div  style="padding: 7px">
    <td><?= $form->field($model, 'description')->textArea(['maxlength' => true, 'rows' => 6,])->label(false) ?>
    </td>
    </div>
</fieldset>

<br>
 <fieldset style ="border: 1px solid black;">
    <p class = "fieldset-background"> Notes </p>
            <div  style="padding: 7px">
                <td><?= $form->field($model, 'notes')->textarea(['rows' => 6])->label(false) ?>
                </td>
           </div>
</fieldset>
 
 

   
    <br>
    <?="";//$form->field($model, 'created_at')->textInput(['value' => date('Y-m-d H:i:s'), 'readOnly' => true]) ?>

    <?="";// $form->field($model, 'updated_at')->textInput(['value' => '', 'readOnly' => true]) ?>

    <br>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
     </div> 

    </fieldset>
    <?php ActiveForm::end(); ?>

   
