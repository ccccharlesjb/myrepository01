<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tickets;

/**
 * TicketsSearch represents the model behind the search form of `app\models\Tickets`.
 */
class TicketsSearch extends Tickets
{
  public $categoriesName;
  public $subCategoriesName;
  
  
  
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ticket_id', 'user_id', 'updated_by', 'category_id', 'sub_category_id', 'priority_id'], 'integer'],
            [['ticket_type', 'description', 'notes','categoriesName','subCategoriesName', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tickets::find();
        $query->joinWith(['categories']);
        $query->joinWith(['subCategories']);
       
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => 
                    [ 'attributes' => 
                                    [
                                        'ticket_id',
                                        'description',
                                        'user_id',
                                        'updated_by',
                                        'categories.name',
                                        'sub_categories.description',
                                    ]

                    ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ticket_id' => $this->ticket_id,
            'user_id' => $this->user_id,
            'updated_by' => $this->updated_by,
            'category_id' => $this->category_id,
            'sub_category_id' => $this->sub_category_id,
            'priority_id' => $this->priority_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            
        ]);

        $query->andFilterWhere(['like', 'ticket_type', $this->ticket_type])
            
            ->andFIlterWhere(['like', 'name', $this->categoriesName])
            ->andFIlterWhere(['like', 'sub_categories.description', $this->subCategoriesName])
            ->andFilterWhere(['like', 'notes', $this->notes]);


        return $dataProvider;
    }
}
