<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tickets".
 *
 * @property string $ticket_id
 * @property string $user_id The user_id of the requestor
 * @property string $updated_by user_id of the last user who updated the ticket
 * @property string $category_id Foreign key to the categories table
 * @property string $sub_category_id Foreign key to sub_categories, list them down based on ticket_type selected
 * @property string $ticket_type Enum type to be used to get the sub_categories (tasks) for the ticket
 * @property string $priority_id
 * @property string $description
 * @property string $notes
 * @property string $created_at
 * @property string $updated_at
 */
class Tickets extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tickets';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'updated_by', 'category_id', 'sub_category_id', 'priority_id'], 'integer'],
            [['user_id','category_id', 'sub_category_id', 'priority_id', 'description'], 'required'],
            [['ticket_type', 'notes'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['description'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ticket_id' => 'Ticket ID',
            'user_id' => 'User ID',
            'updated_by' => 'Updated By',
            'category_id' => 'Category ID',
            'sub_category_id' => 'Sub Category ID',
            'ticket_type' => 'Ticket Type',
            'priority_id' => 'Priority ID',
            'description' => 'Description',
            'notes' => 'Notes',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getCategories()
    {
        return $this->hasOne(Categories::className(), ['category_id' => 'category_id' ]);
    }

    public function getCategoriesName()
    {
        return $this->categories->name;
    }
    public function getSubCategories()
    {
        return $this->hasOne(SubCategories::className(), ['sub_category_id' => 'sub_category_id' ]);
    }

    public function getSubCategoriesName()
    {
        return $this->subCategories->description;
    }

   

   
}
