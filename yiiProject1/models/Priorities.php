<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "priorities".
 *
 * @property int $priority_id
 * @property string $description
 * @property string $hours
 */
class Priorities extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'priorities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hours'], 'number'],
            [['description'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'priority_id' => 'Priority ID',
            'description' => 'Description',
            'hours' => 'Hours',
        ];
    }
}
