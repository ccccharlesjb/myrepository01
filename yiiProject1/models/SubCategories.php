<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sub_categories".
 *
 * @property int $sub_category_id
 * @property string $category_id
 * @property string $ticket_type
 * @property string $description
 * @property string $notes
 * @property string $created_at
 * @property string $updated_at
 */
class SubCategories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sub_categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id'], 'integer'],
            [['ticket_type', 'notes'], 'string'],
            [['created_at'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['description'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sub_category_id' => 'Sub Category ID',
            'category_id' => 'Category ID',
            'ticket_type' => 'Ticket Type',
            'description' => 'Description',
            'notes' => 'Notes',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
