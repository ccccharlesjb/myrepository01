<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\helpers\Security;
use \yii\base\BaseObject;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property string $authKey
 */
class Users extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public $hashPassword = false;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }
   
    


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
        [['first_name', 'last_name', 'email', 'password' /*, 'authKey' */], 'string', 'max' => 120],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'password' => 'Password',
            /*'authKey' => 'authKey', */
        ];
    }

   //Login -------------------------------
   //given functions required by the implemented validatior function
   public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
      //  return static::findOne(['access_token' => $token]);
      throw new NotSupportedException();
    }

    public function getId()
    {
        return $this->user_id;
    }
    
    public function getAuthKey()
    {
       // return $this->authKey;
        return "123232@56";
    } 

    public function validateAuthKey($authKey)
    {
        //return $this->authKey === $authKey;
        return true;
    }

    public static function findByUsername($email)
    {
        return  static::findOne(['email'=> $email]); 
    }

    public function validatePassword($password)
    {
       
        //return $this->password === $password;
        return \Yii::$app -> security ->validatePassword($password, $this -> password);
    }

    
    public function beforeSave($insert)
    {
       
                $this->password = \Yii::$app -> security -> generatePasswordHash($this -> password, 10);
                return true; //this value is important dont remove this.
    }
    

}



