<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\Tickets;
use app\models\Users;
use app\models\Categories;
use app\models\SubCategories;
use app\models\Priorities;
use app\models\TicketsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\SQLDataProvider;

/**
 * TicketsController implements the CRUD actions for Tickets model.
 */
class TicketsController extends Controller
{
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        /*
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];*/

        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','create','delete','update'],
                'rules' => [
                    [
                        'actions' => ['index','create','delete','update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tickets models.
     * @return mixed
     */
    public function actionIndex()
    {

         /*
        $sql = "SELECT tickets.ticket_id,
        tickets.description,
        CONCAT(users.first_name,' ',users.last_name) AS name,
        FROM tickets,
        users
        WHERE tickets.user_id = users.user_id"; */

        //echo Yii::$app->user->identity->email . " ID: " . Yii::$app->user->identity->id;
        $user_ID = Yii::$app->user->identity->user_id;

        $sql = "SELECT tickets.ticket_id,
        tickets.ticket_type,
        tickets.description,
        CONCAT(users.first_name,' ',users.last_name) AS created_by,
        CONCAT ((SELECT users.first_name FROM users WHERE users.user_id = tickets.updated_by),' ',(SELECT users.last_name FROM users WHERE users.user_id = tickets.updated_by)) AS updated_by,
        tickets.priority_id AS priority
        FROM tickets
        LEFT JOIN users ON tickets.user_id = users.user_id";
        
        $userID = Yii::$app->user->identity->id;
        $sqlForUser = "SELECT tickets.ticket_id,
        tickets.ticket_type,
        tickets.description,
        CONCAT(users.first_name,' ',users.last_name) AS created_by,
        CONCAT ((SELECT users.first_name FROM users WHERE users.user_id = tickets.updated_by),' ',(SELECT users.last_name FROM users WHERE users.user_id = tickets.updated_by)) AS updated_by,
        tickets.priority_id AS priority
        FROM tickets
        LEFT JOIN users ON tickets.user_id = users.user_id 
        WHERE users.user_id = $user_ID OR tickets.updated_by = $user_ID";

        $sqlProvider = new SQLDataProvider([
            'sql' => $sql,
            'sort' => [ 'attributes' => [
                'ticket_id',
                'ticket_type',
                'description',
                'created_by',
                'updated_by',
                'priority',
                ]
                
    
                ],
        ]);
        $sqlProviderForUser = new SQLDataProvider([
            'sql' => $sqlForUser,
            'sort' => [ 'attributes' => [
            'ticket_id',
            'ticket_type',
            'description',
            'created_by',
            'updated_by',
            'priority',
            ]
            

            ],
        ]);

            
       
        return $this->render('index', [
            'sqlProviderForUser' => $sqlProviderForUser,
            'sqlProvider' => $sqlProvider,
        ]);
        /*
        $searchModel = new TicketsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]); 
        */
    }

    /**
     * Displays a single Tickets model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'creator_record' => Users::findOne($id),
            'updator_record' => Users::findOne($this->findModel($id)->updated_by),
        ]);
    }

    /**
     * Creates a new Tickets model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $categories_model = Categories::find()->all();
        $sub_categories_model = SubCategories::find()->all();
        $priorities_model = Priorities::find()->all();
        
        $model = new Tickets();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ticket_id]);
        }

        return $this->render('create', [
            'model' => $model,
            'categories_model' => $categories_model,
            'priorities_model' => $priorities_model,
            'sub_categories_model' => $sub_categories_model,
        ]);
    }

    /**
     * Updates an existing Tickets model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    
    public function actionUpdate($id)
    {
        
        $categories_model = Categories::find()->all();
        $sub_categories_model = SubCategories::find()->all();
        $priorities_model = Priorities::find()->all();

       
      
        $selected_record = Tickets::findOne($id);
        
        $ticket_creator = Users::findOne($selected_record->user_id);

    
        $model = $this->findModel($id);

        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ticket_id]);
        }

        return $this->render('update', [
            'model' => $model,
            'categories_model' => $categories_model,
            'priorities_model' => $priorities_model,
            'sub_categories_model' => $sub_categories_model,
            'selected_record' => $selected_record,
            'ticket_creator' => $ticket_creator,
        ]);
    }

    /**
     * Deletes an existing Tickets model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tickets model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Tickets the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tickets::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGetsc($id)
    {

        

        $list_sub_categories_model = SubCategories::find()->
        where(['category_id' => $id])->
        orderBy('sub_category_id ASC')->all();

        if(isset($id))
        foreach($list_sub_categories_model as $list_sub_category) echo "<option value ='" . 
        $list_sub_category->sub_category_id . "'>" . $list_sub_category->description . "</option>";
        else
        echo "<option> - </option>";

    }
    

    public function actionTest()
    {

      
        /*
        $sql = "SELECT tickets.ticket_id,
        tickets.description,
        CONCAT(users.first_name,' ',users.last_name) AS name,
        FROM tickets,
        users
        WHERE tickets.user_id = users.user_id"; */

        //echo Yii::$app->user->identity->email . " ID: " . Yii::$app->user->identity->id;

        $sql = "SELECT tickets.ticket_id,
        tickets.ticket_type,
        tickets.description,
        CONCAT(users.first_name,' ',users.last_name) AS name,
        CONCAT ((SELECT users.first_name WHERE users.user_id = tickets.updated_by),' ', (SELECT users.last_name WHERE users.user_id = tickets.updated_by)) AS updated_by,
        tickets.priority_id AS priority
        FROM tickets
        LEFT JOIN users ON tickets.user_id = users.user_id";
        
        $userID = Yii::$app->user->identity->id;
        $sqlForUser = "SELECT tickets.ticket_id,
        tickets.ticket_type,
        tickets.description,
        CONCAT(users.first_name,' ',users.last_name) AS name,
        CONCAT ((SELECT users.first_name WHERE users.user_id = tickets.updated_by),' ', (SELECT users.last_name WHERE users.user_id = tickets.updated_by)) AS updated_by,
        tickets.priority_id AS priority
        FROM tickets
        LEFT JOIN users ON tickets.user_id = users.user_id 
        WHERE users.user_id = $userID";


        $sqlProvider = new SQLDataProvider([
            'sql' => $sql,
            'sort' => [ 'attributes' => [
                'ticket_id',
                'ticket_type',
                'description',
                'name',
                'updated_by',
                'priority',
                ]
                
    
                ],
        ]);
        $sqlProviderForUser = new SQLDataProvider([
            'sql' => $sqlForUser,
            'sort' => [ 'attributes' => [
            'ticket_id',
            'ticket_type',
            'description',
            'name',
            'updated_by',
            'priority',
            ]
            

            ],
        ]);

              
        $searchModel = new TicketsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        
       
        return $this->render('test', [
            'sqlProviderForUser' => $sqlProviderForUser,
            'sqlProvider' => $sqlProvider,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


}
