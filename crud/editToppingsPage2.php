<?php
include "header.php";
include "footer.php";
require 'models/pizzaClass.php';
session_start();
$obj = new pizzaClass();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edit a Topping</title>
</head>
<body>
<h2> Add a Topping</h2>

<form method="post">
    <label for="topping_name">Topping Name</label>
    <input type="text" name="topping_name" id="topping_name" value="<?=$obj->convert_ID_to_Name($_POST['edit_btn'],1);?>">
    <label for="topping_price">Price</label>
    <input type="text" name="topping_price" id="topping_price" value="<?=$obj->convert_toppingsID_to_value($_POST['edit_btn'],1);?>">

    <?php $categories = $obj->get_categories(); ?>

    <label for="topping_category">Category</label>
        <select name="topping_category">

            <?php foreach($categories as $cat_index=> $cat_content): ?>

                <option value="<?=$categories[$cat_index]['category_id'];?>"> <?=$categories[$cat_index]['category_name'];?> </option>
                
            <?php endforeach; ?>
        </select>

    </br> </br> <input type="submit" name="add_btn" value="Edit" onclick="myFunction()">
</form>

<?php

if(isset($_POST['add_btn']))
    if (strlen(trim($_POST['topping_name'])) > 0 && strlen(trim($_POST['topping_price'])) > 0 )
    {
      
    }
    else
    echo "Please fill out all the fields ";

?>

<script>
function myFunction() {
    alert("Topping Successfully Edited");
}
</script>


</body>
</html>