<?php

class Pizza
{
   
    public $array_for_display;

    public $pizzaArray =
[ ///multipliaer
'size' => [
'12_inch' => 1,
'18_inch' => 1.6,
'22_inch' => 1.9,
],
'vegetable_toppings' => [
'roma_tomatoes' => 60.00,
'red_onion' => 23.00,
'green_pepper' => 40.00,
'chilly' => 35.00,
'mushroom' => 55.00,
'grilled_zucchini' => 35.00,
'green_olives' => 65.00
],
'non_vegetable_toppings' => [
'pepperoni' => 88.00,
'bacon' => 120.00,
'chicken' => 90.00,
'steak_strip' => 140.00,
'italian_ham' => 50.00,
'chorizo_sausage' => 70.00
],
'cheese' => [
'cheese' => 70.00,
'feta_cheese' => 95.00,
'parmesan_cheese' => 78.00,
'extra_cheese' => 50.00,
'four_cheese' => 150.00,
'goat_cheese' => 90.00,
'mozzarella' => 80.00,
'dairy_free_cheese' => 140.00
]
];


    public function computeTotalOrder($input_array,$pizza_multiplier)
    {
        
        $total=0;
        
        foreach($input_array as $newPizzaArray)
        { 
            $total+= $this->convert_to_value($newPizzaArray)*$this->convert_to_value($pizza_multiplier);
        }
        
       return $total;
        

    }


    


    public function viewIngredientsList($input_array,$pizza_multiplier)
    {

           

        $output="";
        foreach($input_array as $newPizzaArray)
        {
                $space_removed= str_replace("_"," ",$newPizzaArray);
               // echo"TEST ".$this->check_for_header($input_array);
            $output.="</br> {$space_removed}  &nbsp; &nbsp; &nbsp; PHP ".$this->convert_to_value($newPizzaArray)*$this->convert_to_value($pizza_multiplier);
        }
        return $output;
        
    }


    public function get_receipt($input_array,$price_multiplier=1)
    {
        $arr_output=[];
        $print_header = true;
        $output ="";
        $header ="";
        $items ="";
        foreach($this->pizzaArray as $category_name => $category_content)
        {
            
            if($category_name == "size")
                        $arr_output [$category_name][$price_multiplier]= "".$price_multiplier;

            foreach($category_content as $item_name => $item_value)
                {
               
                    foreach($input_array as $new_input_array)
                    {
                        
                        if($new_input_array == $item_name)
                        {
         
                            $arr_output [$category_name][$item_name]= str_pad("" . $item_name,50,".") .
                            "PHP" . $this->convert_to_value($item_name) * 
                            $this->convert_to_value($price_multiplier) . ".</br>";
                         }

                    }
                
                
                }


        }
        
        return $arr_output;

    }


    public function convert_to_value($input)
    {
    
        $return_value=0;
        foreach($this->pizzaArray as $cat_name => $cat_content)
        {
            foreach($cat_content as $index02 => $value02)
            {
               
                if($input==$index02)
                {
                    
                    $return_value = $value02;
                }
            }
        }
            
        return $return_value;
    }    
        

    public function removeSpaces($input)
    {
        return str_replace('_',' ',$input);
    }


    public function get_array_for_display($input_array)
    {
        $this->array_for_display = $input_array;
      

    }



}

